<?php
    namespace Api\DB;
    require "params.php";

    use Api\Controller\ThrowControl;
    use PDO;

    /**
     * Manejo de datos y conexión
     */
    class Db extends ThrowControl{
        private $db = '';
        private $hs = '';
        private $usr = '';
        private $pas = '';
        private $con = null;
        protected $resultData = array();

        function __construct()
        {
            $this->db = CURRENT_DATABASE;//nombre database
            $this->usr = CURRENT_USER;//usuario db
            $this->pas = CURRENT_PASS;//password db
            $this->hs = CURRENT_HOST;// host que apunta
            $this->generar_conexion();//abrimos la conexion
        }

        private function generar_conexion(){
            $datos["result"]=false;
            try {                
                $this->con = new PDO(
                    "mysql:dbname={$this->db};host={$this->hs}", 
                    $this->usr, 
                    $this->pas,
                    [
                        PDO::ATTR_EMULATE_PREPARES => false, 
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    ]
                );
                $datos = $this->cod_response(200);
            } catch (\Exception $e){      
                $datos["data"] = $e->getMessage();
            }
            $this->resultData = $datos;
        }

        protected function clear(){
            $this->con = null;
        }

        protected function actualiza_productos($id, $nombre, $precio, $referencia, $peso, $categoria){
            $datos = $this->cod_response(407);
            try {
                $stmt = $this->con->prepare(SQL_UPDATE_PRODUCTO);
                $v1 = $nombre;
                $v2 = $precio;
                $v3 = $referencia;
                $v4 = $peso;
                $v5 = $categoria;
                $v6 = $id;
                if($stmt->execute(array($v1, $v2, $v3, $v4, $v5, $v6))){
                    $datos = $this->cod_response(200);
                }
                
            } catch (\Throwable $th) {
                $datos["data"] = $th->getMessage();
            }            
            $this->resultData = $datos;
        }

        protected function insertar_producto($nombre, $precio, $referencia, $peso, $categoria ){
            $datos = $this->cod_response(407);
            if($this->resultData["result"]){
                try {
                    $stmt = $this->con->prepare(SQL_CREATE_PRODUC);                
                    $v1 = $nombre;
                    $v2 = $precio;
                    $v3 = $referencia;
                    $v4 = $peso;
                    $v5 = $categoria;
                    $v6 = $GLOBALS["fechaSystem"];
                    if($stmt->execute(array($v1, $v2, $v3, $v4, $v5, $v6))){
                        $datos = $this->cod_response(200);
                    }
                } catch (\Throwable $th) {
                    $datos["data"] = $th->getMessage();
                }                
            }           
            $this->resultData = $datos;
        }

        protected function listar_categorias(){
            $datos = $this->cod_response(406);
            try {
                $stmt = $this->con->prepare(SQL_VER_CATEGORIAS);
                if($stmt->execute()){
                    if($stmt->rowCount()>0){
                        $datos = $this->cod_response(200);
                        $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                        for ($i=0; $i <count($response) ; $i++) { 
                            if($response[$i]["id"]!=null){
                                array_push($datos["data"], 
                                    array(
                                        "id"=>$response[$i]["id"],
                                        "nombre"=>$response[$i]["nombre"],
                                        "estado"=>$response[$i]["estado"]
                                    )
                                );
                            }else{
                                $datos["result"]=false;
                                break;
                            }
                        }
                    }                
                }
            } catch (\Throwable $th) {
                $datos["data"] = $th->getMessage();
            }
            $this->resultData = $datos;
        }

        protected function listar_productos(){
            $datos = $this->cod_response(406);
            try {
                $stmt = $this->con->prepare(SQL_VER_PRODUCTOS);
                if($stmt->execute()){
                    if($stmt->rowCount()>0){
                        $datos = $this->cod_response(200);
                        $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                        for ($i=0; $i <count($response) ; $i++) { 
                            if($response[$i]["id"]!=null){
                                array_push($datos["data"], 
                                    array(
                                        "id"=>$response[$i]["id"],
                                        "nombre_producto"=>$response[$i]["nombre_producto"],
                                        "precio"=>$response[$i]["precio"],
                                        "referencia"=>$response[$i]["referencia"],
                                        "peso"=>$response[$i]["peso"],
                                        "categoria"=>$response[$i]["categoria"],
                                        "fecha_creacion"=>$response[$i]["fecha_creacion"]
                                    )
                                );
                            }else{
                                $datos["result"]=false;
                                break;
                            }
                        }
                    }                
                }
            } catch (\Throwable $th) {
                $datos["data"] = $th->getMessage();
            }
            $this->resultData = $datos;
        } 
        
        protected function elimina_productos($id){
            $datos = $this->cod_response(406);
            try {
                $stmt = $this->con->prepare(SQL_DELETE_PRODUCTO);
                $v1 = $id;
                if($stmt->execute(array($v1))){
                    $datos = $this->cod_response(200);
                }
                
            } catch (\Throwable $th) {
                $datos["data"] = $th->getMessage();
            }            
            $this->resultData = $datos;
        }
        protected function listar_existencias($id){
            $datos = $this->cod_response(406);
            try {
                $stmt = $this->con->prepare(SQL_VER_STOCK);
                $v1 = $id;
                if($stmt->execute(array($v1))){
                    if($stmt->rowCount()>0){
                        $datos = $this->cod_response(200);
                        $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                        for ($i=0; $i <count($response) ; $i++) { 
                            if($response[$i]["id"]!=null){
                                array_push($datos["data"], 
                                    array(
                                        "id"=>$response[$i]["id"],
                                        "ingreso"=>$response[$i]["ingreso"],
                                        "egreso"=>$response[$i]["egreso"],
                                        "fecha"=>$response[$i]["fecha"]
                                    )
                                );
                            }else{
                                $datos["result"]=false;
                                break;
                            }
                        }
                    }else{
                        $datos = $this->cod_response(200, []);
                    }             
                }
            } catch (\Throwable $th) {
                $datos["data"] = $th->getMessage();
            }
            $this->resultData = $datos;
        }
        protected function insertar_existencia($cantidad, $tipo, $id){
            $datos = $this->cod_response(407);
            if($this->resultData["result"]){
                try {
                    $stmt = $this->con->prepare(SQL_CREATE_STOCK); 
                    $ingreso = $cantidad;
                    $egreso = 0;
                    if($tipo==="egreso"){
                        $ingreso = 0;
                        $egreso = $cantidad;
                    }               
                    $v1 = $ingreso;
                    $v2 = $egreso;
                    $v3 = $id;
                    $v4 = $GLOBALS["fechaSystem"];
                    if($stmt->execute(array($v1, $v2, $v3, $v4))){
                        $datos = $this->cod_response(200);
                    }
                } catch (\Throwable $th) {
                    $datos["data"] = $th->getMessage();
                }                
            }           
            $this->resultData = $datos;
        }
        protected function total_existencias($id){
            $datos = $this->cod_response(407);
            if($this->resultData["result"]){
                try {
                    $stmt = $this->con->prepare(SQL_TOTAL_STOCK);
                    $v1 = $id;
                    if($stmt->execute(array($v1))){
                        $datos = $this->cod_response(200);
                        $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                        for ($i=0; $i <count($response) ; $i++) { 
                            if($response[$i]["total_existencias"]!=null){
                                array_push($datos["data"], 
                                    array(
                                        "total_existencias"=>$response[$i]["total_existencias"]
                                    )
                                );
                            }else{
                                $datos["result"]=false;
                                break;
                            }
                        }
                    }
                } catch (\Throwable $th) {
                    $datos["data"] = $th->getMessage();
                }                
            }           
            $this->resultData = $datos;
        }
        protected function saber_datos_producto($id){
            $datos = $this->cod_response(407);
            if($this->resultData["result"]){
                try {
                    $stmt = $this->con->prepare(SQL_PRODUCTO_UNICO);
                    $v1 = $id;
                    if($stmt->execute(array($v1))){
                        $datos = $this->cod_response(200);
                        $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                        for ($i=0; $i <count($response) ; $i++) { 
                            if($response[$i]["nombre_producto"]!=null){
                                array_push($datos["data"], 
                                    array(
                                        "nombre_producto"=>$response[$i]["nombre_producto"],
                                        "precio"=>$response[$i]["precio"],
                                        "referencia"=>$response[$i]["referencia"],
                                        "peso"=>$response[$i]["peso"],
                                        "categoria"=>$response[$i]["categoria"],
                                        "fecha_creacion"=>$response[$i]["fecha_creacion"],
                                    )
                                );
                            }else{
                                $datos["result"]=false;
                                break;
                            }
                        }
                    }
                } catch (\Throwable $th) {
                    $datos["data"] = $th->getMessage();
                }                
            }
            $this->resultData = $datos;
        }
        protected function saber_ultima_venta($comprador, $totalVenta){
            $datos = $this->cod_response(407);
            if($this->resultData["result"]){
                try {
                    $stmt = $this->con->prepare(SQL_ULTIMA_VENTA);
                    $v1 = $comprador; 
                    $v2 = $totalVenta; 
                    $v3 = $GLOBALS["fechaSystem"]; 
                    if($stmt->execute(array($v1, $v2, $v3))){
                        $datos = $this->cod_response(200);
                        $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                        for ($i=0; $i <count($response) ; $i++) { 
                            if($response[$i]["id_venta"]!=null){
                                array_push($datos["data"], 
                                    array(
                                        "id_venta"=>$response[$i]["id_venta"]
                                    )
                                );
                            }else{
                                $datos["result"]=false;
                                break;
                            }
                        }
                    }
                } catch (\Throwable $th) {
                    $datos["data"] = $th->getMessage();
                }                
            }
            $this->resultData = $datos;
        }
        protected function insertar_venta($comprador, $totalVenta){
            $datos = $this->cod_response(407);
            if($this->resultData["result"]){
                try {
                    $stmt = $this->con->prepare(SQL_GENERAR_VENTA); 
                    $v1 = $comprador;
                    $v2 = $totalVenta;
                    $v3 = $GLOBALS["fechaSystem"];
                    if($stmt->execute(array($v1, $v2, $v3))){
                        $datos = $this->cod_response(200);
                    }
                } catch (\Throwable $th) {
                    $datos["data"] = $th->getMessage();
                }                
            }           
            $this->resultData = $datos;
        }
        protected function insertar_venta_detalle($idProducto, $precioVenta, $cantidad, $idVenta){
            $datos = $this->cod_response(407);
            if($this->resultData["result"]){
                try {
                    SQL_GENERAR_VENTA_DETALLE;
                    $stmt = $this->con->prepare(SQL_GENERAR_VENTA_DETALLE); 
                    $v1 = $idVenta; 
                    $v2 = $idProducto; 
                    $v3 = $precioVenta; 
                    $v4 = $cantidad; 
                    $v5 = $GLOBALS["fechaSystem"]; 
                    if($stmt->execute(array($v1, $v2, $v3, $v4, $v5))){
                        $datos = $this->cod_response(200);
                    }
                } catch (\Throwable $th) {
                    $datos["data"] = $th->getMessage();
                }                
            }           
            $this->resultData = $datos;
        }
        protected function listar_maximos(){
            $datos = $this->cod_response(406);
            try {
                $stmt = $this->con->prepare(SQL_VER_MAXIMOS);
                if($stmt->execute()){
                    $datos = $this->cod_response(200);
                    $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                    for ($i=0; $i <count($response) ; $i++) { 
                        if($response[$i]["prod"]!=null){
                            array_push($datos["data"], 
                                array(
                                    "producto"=>$response[$i]["prod"],
                                    "total"=>$response[$i]["maximo"]
                                )
                            );
                        }else{
                            $datos["result"]=false;
                            break;
                        }
                    }            
                }
            } catch (\Throwable $th) {
                $datos["data"] = $th->getMessage();
            }
            $this->resultData = $datos;
        }
        protected function listar_mas_vendidos(){
            $datos = $this->cod_response(406);
            try {
                $stmt = $this->con->prepare(SQL_VER_MAS_VENDIDO);
                if($stmt->execute()){
                    $datos = $this->cod_response(200);
                    $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                    for ($i=0; $i <count($response) ; $i++) { 
                        if($response[$i]["prod"]!=null){
                            array_push($datos["data"], 
                                array(
                                    "producto"=>$response[$i]["prod"],
                                    "total"=>$response[$i]["maximo"]
                                )
                            );
                        }else{
                            $datos["result"]=false;
                            break;
                        }
                    }            
                }
            } catch (\Throwable $th) {
                $datos["data"] = $th->getMessage();
            }
            $this->resultData = $datos;
        }
    }
?>