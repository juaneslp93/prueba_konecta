<?php
    //conexion
    if(!$sandbox){
        @define("CURRENT_HOST","localhost");
        @define("CURRENT_DATABASE","prod_prueba_konecta");
        @define("CURRENT_USER","user_API");
        @define("CURRENT_PASS","*CPQ%@gY9lsw");
    }else{
        @define("CURRENT_HOST","localhost");
        @define("CURRENT_DATABASE","prueba_konecta");
        @define("CURRENT_USER","root");
        @define("CURRENT_PASS","");
    }

    /* SQL */
    #GENERAL
    @define("SQL_VALIDATE_TABLE","SHOW TABLES LIKE ");
    //consultas
    @define("SQL_CREATE_PRODUC","INSERT INTO productos (nombre_producto, precio, referencia, peso, categoria, fecha_creacion) VALUES (?,?,?,?,?,?) ");
    @define("SQL_VER_CATEGORIAS","SELECT id, nombre, estado FROM categoria");
    @define("SQL_VER_PRODUCTOS","SELECT id, nombre_producto, precio, referencia, peso, (SELECT nombre FROM categoria WHERE productos.id = categoria.id) as categoria, fecha_creacion FROM productos WHERE eliminado='0'");
    @define("SQL_UPDATE_PRODUCTO","UPDATE productos SET nombre_producto=?, precio=?, referencia=?, peso=?, categoria=? WHERE id=? ");
    @define("SQL_DELETE_PRODUCTO","UPDATE productos SET eliminado='1' WHERE id=? ");
    @define("SQL_VER_STOCK","SELECT id, ingreso, egreso, fecha FROM stock WHERE id_producto=?");
    @define("SQL_CREATE_STOCK","INSERT INTO stock (ingreso, egreso, id_producto, fecha) VALUES (?,?,?,?) ");
    @define("SQL_TOTAL_STOCK"," SELECT sum(ingreso-egreso) AS total_existencias FROM stock WHERE id_producto = ? ");
    @define("SQL_PRODUCTO_UNICO","SELECT nombre_producto, precio, referencia, peso, (SELECT nombre FROM categoria WHERE productos.id = categoria.id) as categoria, fecha_creacion FROM productos WHERE eliminado='0' AND id=?");
    @define("SQL_GENERAR_VENTA","INSERT INTO ventas (comprador, total_venta, fecha) VALUES (?,?,?) ");
    @define("SQL_GENERAR_VENTA_DETALLE","INSERT INTO ventas_detalle (id_venta, id_producto, precio_venta, cant, fecha_registro) VALUES (?,?,?,?,?) ");
    @define("SQL_ULTIMA_VENTA","SELECT max(id) as id_venta FROM ventas WHERE comprador=? AND total_venta=? AND fecha=?");
    @define("SQL_VER_MAXIMOS","SELECT prod, max(data) maximo FROM ( SELECT sum(s.ingreso-s.egreso) data, (SELECT nombre_producto FROM productos as pp WHERE pp.id = p.id) prod FROM stock as s INNER JOIN productos as p ON p.id = s.id_producto GROUP BY p.id )c1;");
    @define("SQL_VER_MAS_VENDIDO","SELECT prod, max(data) maximo FROM ( SELECT (SELECT nombre_producto FROM productos as pp WHERE pp.id = v.id_producto) prod, SUM(cant) as data FROM ventas_detalle as v GROUP BY id_producto )c1;");
?>