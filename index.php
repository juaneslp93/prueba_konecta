<?php 
require __DIR__."/resources/resources.php";
use Api\Controller\Controller;
require __DIR__."/resources/badCode.php";
$ctr = new Controller();
if(empty($params)){
    header("Location: productos");
    exit;
}
$ctr->validar_request($request, $params);
require_once __DIR__."/public/response.php";
?>