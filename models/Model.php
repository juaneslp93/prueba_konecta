<?php
    namespace Api\Model;
    use Api\Controller\ThrowControl;
    use Api\DB\Db;
    /**
     * Modelado y manejo de datos
     */
    class Model extends Db
    {
        protected $datos = array();
        private $db = null;
        function __construct(){}

        protected function insert_producto($nombre, $precio, $referencia, $peso, $categoria){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->insertar_producto($nombre, $precio, $referencia, $peso, $categoria);
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
                     
        }

        protected function lista_categorias(){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->listar_categorias();
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
        }

        protected function lista_productos(){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->listar_productos();
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
        }
        protected function actualizar_productos($id, $nombre, $precio, $referencia, $peso, $categoria){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->actualiza_productos($id, $nombre, $precio, $referencia, $peso, $categoria);
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
        }
        protected function eliminar_productos($id){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->elimina_productos($id);
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
        }
        protected function lista_stock($id){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->listar_existencias($id);
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
        }
        protected function insert_existencia($cantidad, $tipo, $id){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->insertar_existencia($cantidad, $tipo, $id);
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
                     
        }
        protected function registrar_venta($comprador, $cantidad, $idProducto){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->total_existencias($idProducto);
                if($this->db->resultData["data"][0]["total_existencias"]>0 && $this->db->resultData["data"][0]["total_existencias"]>=$cantidad){
                    $this->db->saber_datos_producto($idProducto);
                    if($this->db->resultData["result"]){
                        $totalVenta = $this->db->resultData["data"][0]["precio"]*$cantidad;
                        $precioVenta=$this->db->resultData["data"][0]["precio"];
                        $this->db->insertar_venta($comprador, $totalVenta);
                        if($this->db->resultData["result"]){
                            $this->db->saber_ultima_venta($comprador,$totalVenta);
                            if($this->db->resultData["result"]){
                                $idVenta = $this->db->resultData["data"][0]["id_venta"];
                                $this->db->insertar_venta_detalle($idProducto, $precioVenta, $cantidad, $idVenta);
                                if($this->db->resultData["result"]){
                                    $this->db->insertar_existencia($cantidad, "egreso", $idProducto);
                                }
                            }
                        }
                    }   
                }else{
                    $this->db->resultData = $this->cod_response(404,[], "La cantidad seleccionada supera el monto de existencias");
                }
            }
            $this->db->clear();
            $this->datos = $this->db->resultData;
                     
        }
        protected function lista_maximos(){
            $this->db = new Db();
            if($this->db->resultData["result"]){
                $this->db->listar_maximos();
                if($this->db->resultData["result"]){
                    $productoMax=$this->db->resultData["data"][0]["producto"];
                    $total=$this->db->resultData["data"][0]["total"];
                    $this->db->listar_mas_vendidos();
                    if($this->db->resultData["result"]){
                        $productoVenta=$this->db->resultData["data"][0]["producto"];
                        $maxVenta=$this->db->resultData["data"][0]["total"];
                    }
                }
            }
            $this->db->resultData["data"] = array("maximo"=>array("producto"=>$productoMax, "total"=>$total),"mas_vendido"=>array("producto"=>$productoVenta, "total"=>$maxVenta));

            $this->db->clear();
            $this->datos = $this->db->resultData;
        }
    }
    
?>