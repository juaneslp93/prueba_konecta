<?php 
    namespace Api\Controller;
    /**
     * Control y manejo de errores internos
     */
    class ThrowControl
    {
        function __construct(){}
        /**
         * Control de errores por código
         */
        public function cod_response($code, $data=array(), $complement="", ){
            if(is_array($complement)){
                $complement = "";
            }
            switch ($code) {
                case 407:
                    return  array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "Failed to insert the data. $complement");
                break;
                case 406:
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "No results found, check your parameters. $complement");
                break;
                case 405:
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "The requested method is known to the server but has been disabled and cannot be used. $complement");
                break;
                case 404:
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "Not found. $complement");
                break;       
                case 403:
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "Error in the request. $complement");
                break;
                case 402:
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "Method does not apply. $complement");
                break;
                case 401:
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "Authentication is failed. $complement");
                break;
                case 'info_advanced':
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"info", "help" => "Data is no not compatible, require clausule '=' for finder. Example: estado=1.  $complement");
                break;
                case 200:
                    return array("result"=>true, "statusCode"=>$code, "data"=>$data, "error"=>"", "help" => "");
                break;
                
                default:
                    return array("result"=>false, "statusCode"=>$code, "data"=>array(), "error"=>"failed", "help" => "Request not recognized. $complement");
                break;
            }            
        }
    }
    
?>