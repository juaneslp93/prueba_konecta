<?php
    namespace Api\Controller;
    use Api\Model\Model;
    /**
     * Control y manejo de solicitudes
     */
    class Controller extends Model{
        private $mdl = null;
        private $method = null;
        private $type = null;
        public $resultData;
        public $TOKEN;
        public $APIKEY;
        function __construct(){}
        /**
         * Validación de la solicitud
         */
        public function validar_request($request, $params){
            switch ($request) {
                case 'POST':
                    $this->method = $params;
                    $this->type = 'post';
                break;
                case 'GET':                    
                    $this->method = $params;
                    $this->type = 'get';
                break;
                case 'BAD_REQUEST':
                    $this->respuesta_http(200);
                    $this->json_data($this->cod_response($params));
                    die($this->resultData);
                break;
                default:
                    $this->respuesta_http(200);
                    $this->json_data($this->cod_response(405));
                    die($this->resultData);
                break;
            }

            $this->validar_metodo();
            $this->continuar_a_solicitud();
        }
        /**
         * Escapado de cadenas
         */
        private function escapar($key){
            if($this->method[$key]){
                $this->method[$key] = str_replace('"', "", $this->method[$key]);
            }
        }
        /**
         * Validación de métodos permitidos
         */
        private function validar_metodo(){
            $this->escapar("method");
            if(
                isset($this->method["method"]) && 
                !empty($this->method["method"]) && 
                $this->method["method"] != null
            ){
                $metodosPermitidos = array(
                    'ver_categorias',
                    'actualizar_producto',
                    'nuevo_producto',
                    'ver_productos',
                    'eliminar_producto',
                    'lista_stock',
                    'nuevo_stock',
                    'cargar_productos',
                    'generar_venta',
                    'maximos'
                );
                if(!in_array($this->method["method"], $metodosPermitidos)){
                    $this->respuesta_http(200);
                    $this->json_data($this->cod_response(403));
                    die($this->resultData);
                }
            }else{
                $this->respuesta_http(200);
                $this->json_data($this->cod_response(406));
                die($this->resultData);
            }          
        }
        /**
         * Control de casos de uso
         */
        private function continuar_a_solicitud(){
            switch ($this->method["method"]) {
                case 'nuevo_producto':
                    if($this->type=='post'){
                        $this->insert_new_producto();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'ver_categorias':
                    if($this->type=='get'){
                        $this->ver_categorias();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'ver_productos':
                    if($this->type=='get'){
                        $this->ver_productos();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'actualizar_producto':
                    if($this->type=='post'){
                        $this->actualizar_producto();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'eliminar_producto':
                    if($this->type=='post'){
                        $this->eliminar_producto();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'lista_stock':
                    if($this->type=='post'){
                        $this->ver_existencias();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'nuevo_stock':
                    if($this->type=='post'){
                        $this->registrar_existencias();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'cargar_productos':
                    if($this->type=='get'){
                        $this->cargar_productos();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'generar_venta':
                    if($this->type=='post'){
                        $this->generar_venta();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                case 'maximos':
                    if($this->type=='get'){
                        $this->obtener_maximos();
                    }else{
                        $this->respuesta_http(200);
                        $this->json_data($this->cod_response(406));
                        die($this->resultData);
                    }
                break;
                default:
                    $this->respuesta_http(200);
                    $this->json_data($this->cod_response(406));
                    die($this->resultData);
                break;
            }
        }
        /**
         * Conversión de datos a formato JSON
         */
        private function json_data($dat=array()){
            $dataRead = ((!empty($dat))?$dat:$this->mdl->datos);
            $this->resultData = json_encode($dataRead, JSON_HEX_QUOT || JSON_ERROR_UTF8 || JSON_OBJECT_AS_ARRAY || JSON_PARSER_NOTSTRICT);
        }
        /**
         * Configuración de respuesta HTTP
         */
        private function respuesta_http($codigo){
            http_response_code($codigo);
        }
        /**
         * Crear producto
         */
        protected function insert_new_producto(){
            $this->escapar("nombre");
            $this->escapar("precio");
            $this->escapar("referencia");
            $this->escapar("peso");
            $this->escapar("categoria");
            if(
                (isset($this->method["nombre"]) && isset($this->method["precio"]) && isset($this->method["referencia"]) && isset($this->method["peso"]) && isset($this->method["categoria"])) && 
                (!empty($this->method["nombre"]) && !empty($this->method["precio"]) && !empty($this->method["referencia"]) && !empty($this->method["peso"]) && !empty($this->method["categoria"])) &&
                ($this->method["nombre"] != null && $this->method["precio"] != null  && $this->method["referencia"] != null  && $this->method["peso"] != null && $this->method["categoria"] != null)
            ){
                $this->mdl = new Model();
                $this->mdl->insert_producto($this->method["nombre"], $this->method["precio"], $this->method["referencia"], $this->method["peso"], $this->method["categoria"]);
                $this->respuesta_http(200);
                $this->json_data();
            }else{
                $this->respuesta_http(200);
                $this->json_data($this->cod_response(406));
                die($this->resultData);
            }   
                     
        }
        /**
         * Consulta general de categorias
         */
        protected function ver_categorias(){
            $this->mdl = new Model();
            $this->mdl->lista_categorias();
            $this->respuesta_http(200);
            $this->json_data();            
        }
        /**
         * Consulta general de productos
         */
        protected function ver_productos(){
            $this->mdl = new Model();
            $this->mdl->lista_productos();
            $this->respuesta_http(200);
            $this->json_data();            
        }
        /**
         * Actualización de productos
         */
        protected function actualizar_producto(){
            $this->escapar("id");
            $this->escapar("nombreEdit");
            $this->escapar("PrecioEdit");
            $this->escapar("referenciaEdit");
            $this->escapar("pesoEdit");
            $this->escapar("categoriaEdit");
            if(
                (isset($this->method["id"]) && isset($this->method["nombreEdit"]) && isset($this->method["precioEdit"]) && isset($this->method["referenciaEdit"]) && isset($this->method["pesoEdit"]) && isset($this->method["categoriaEdit"])) && 
                (!empty($this->method["id"]) && !empty($this->method["nombreEdit"]) && !empty($this->method["precioEdit"]) && !empty($this->method["referenciaEdit"]) && !empty($this->method["pesoEdit"]) && !empty($this->method["categoriaEdit"])) &&
                ($this->method["id"] != null && $this->method["nombreEdit"] != null && $this->method["precioEdit"] != null  && $this->method["referenciaEdit"] != null  && $this->method["pesoEdit"] != null && $this->method["categoriaEdit"] != null)
            ){
                $this->mdl = new Model();
                $this->mdl->actualizar_productos($this->method["id"],$this->method["nombreEdit"], $this->method["precioEdit"], $this->method["referenciaEdit"], $this->method["pesoEdit"], $this->method["categoriaEdit"]);
                $this->respuesta_http(200);
                $this->json_data();
            }else{
                $this->respuesta_http(200);
                $this->json_data($this->cod_response(406));
                die($this->resultData);
            }
        }
        /**
         * Eliminación de productos
         */
        protected function eliminar_producto(){
            $this->escapar("id");
            if(
                (isset($this->method["id"]) ) && 
                (!empty($this->method["id"]) ) &&
                ($this->method["id"] != null )
            ){
                $this->mdl = new Model();
                $this->mdl->eliminar_productos($this->method["id"]);
                $this->respuesta_http(200);
                $this->json_data();
            }else{
                $this->respuesta_http(200);
                $this->json_data($this->cod_response(406));
                die($this->resultData);
            }
        }
        /**
         * Consulta general de existencias
         */
        protected function ver_existencias(){
            $this->escapar("id");
            if(
                (isset($this->method["id"]) ) && 
                (!empty($this->method["id"]) ) &&
                ($this->method["id"] != null )
            ){
                $this->mdl = new Model();
                $this->mdl->lista_stock($this->method["id"]);
                $this->respuesta_http(200);
                $this->json_data();
            }else{
                $this->respuesta_http(200);
                $this->json_data($this->cod_response(406));
                die($this->resultData);
            }
        }
        /**
         * Crear producto
         */
        protected function registrar_existencias(){
            $this->escapar("cantidad");
            $this->escapar("tipo");
            $this->escapar("id");
            if(
                (isset($this->method["cantidad"]) && isset($this->method["tipo"]) && isset($this->method["id"]) ) && 
                (!empty($this->method["cantidad"]) && !empty($this->method["tipo"]) && !empty($this->method["id"]) ) &&
                ($this->method["cantidad"] != null && $this->method["tipo"] != null  && $this->method["id"] != null )
            ){
                $this->mdl = new Model();
                $this->mdl->insert_existencia($this->method["cantidad"], $this->method["tipo"], $this->method["id"]);
                $this->respuesta_http(200);
                $this->json_data();
            }else{
                $this->respuesta_http(200);
                $this->json_data($this->cod_response(406));
                die($this->resultData);
            }   
                     
        }
        /**
         * Consulta general de productos
         */
        protected function cargar_productos(){
            $this->mdl = new Model();
            $this->mdl->lista_productos();
            $this->respuesta_http(200);
            $this->json_data();            
        }
        /**
         * Crear venta
         */
        protected function generar_venta(){
            $this->escapar("comprador");
            $this->escapar("cantidad");
            $this->escapar("id");
            if(
                (isset($this->method["comprador"]) && isset($this->method["cantidad"]) && isset($this->method["id"]) ) && 
                (!empty($this->method["comprador"]) && !empty($this->method["cantidad"]) && !empty($this->method["id"])) &&
                ($this->method["comprador"] != null && $this->method["cantidad"] != null  && $this->method["id"] != null )
            ){
                $this->mdl = new Model();
                $this->mdl->registrar_venta($this->method["comprador"], $this->method["cantidad"], $this->method["id"]);
                $this->respuesta_http(200);
                $this->json_data();
            }else{
                $this->respuesta_http(200);
                $this->json_data($this->cod_response(406));
                die($this->resultData);
            }        
        }
        protected function obtener_maximos(){
            $this->mdl = new Model();
            $this->mdl->lista_maximos();
            $this->respuesta_http(200);
            $this->json_data();            
        }
    }
    
?>