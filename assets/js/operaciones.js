function procesarTabla(data){
    let cabezera = [];
    let cuerpo = [];
    if(data.length>0){
        for (let idxHead = 0; idxHead < data.length; idxHead++) {
            const header = data[idxHead];            
            cabezera=Object.keys(header); 
            break;
        }
        cuerpo = [...Object.values(data)];
    }
    return tabla = {cabezera,cuerpo};
}

function openEdit(data){
    data = JSON.parse(data)
    const form = procesarFormularioEdicionProducto(data );
    $("#contentEdit").html(form)
}
function openDelete(data){
    data = JSON.parse(data);
    Swal.fire({
        title: '¿Está seguro de eliminar este producto?',
        text:'Los cambios son irreversibles',
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: 'Si, estoy seguro',
        denyButtonText: 'No, vuelve a la tabla',
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: "post",
                url: $("#idEdit").val(),
                data: {method:"eliminar_producto", id:data.id},
                dataType: "json",
                success: function (response) {
                    if(response.result){
                        Swal.fire('Proceso exitoso', 'El producto se ha eliminado . '+response.help, 'success')
                    }else{
                      Swal.fire('Proceso detenido', 'El producto no se eliminó. '+response.help, 'warning')
                    }
                },
                error: function(err){
                Swal.fire('Error inesperado', JSON.stringify(err), 'danger')
                }
            });
        }
    })
}
function generarTabla(tabla, acciones=true){
    var head = '<tr>';
    for (let idxH = 0; idxH < tabla.cabezera.length; idxH++) {
        const header = tabla.cabezera[idxH];
        head += `<th>${header}</th>`;
    }
    if(acciones){
        head += '<th>Acciones</th>';
    }
    head += '</tr>';
    var body = '<tr>';
    if(tabla.cuerpo.length===0){
        body += `<tr> <td colSpan=`+((tabla.cabezera.length>0)?tabla.cabezera.length:1)+`> Sin datos</td></tr>`;
    }else{
        for (let idxBody = 0; idxBody < tabla.cuerpo.length; idxBody++) {
            //tabla
            const eleBody = tabla.cuerpo[idxBody];
            let td = '';
            // let element = [];
            for (let idxProps = 0; idxProps < tabla.cabezera.length; idxProps++) {
                const elHead = tabla.cabezera[idxProps];
                // element.push(idxProps)
                if(elHead==="estado"){
                    if(eleBody[elHead]==="1"){
                        td += `<td> <i class="fa fa-check text-success"></i></td>`;
                    }else{
                        td += `<td> <i class="fa fa-close text-danger"></i></td>`;
                    }                
                }else{
                    td += `<td>${eleBody[elHead]}</td>`;
                }
                // element += `eleBody[elHead]`;
            }
            let cri = JSON.stringify(eleBody)
            cri = cri.replace(/"/g, "\\\"");  //escapamos la comilla
            if(acciones){
                td += `<td>
                        <button
                            id="btnAcciones${idxBody}" 
                            class="btn btn-success btn-sm" 
                            data-toggle="modal" 
                            data-target="#mdlEdit"
                            onclick = 'openEdit("`+cri+`")'
                            title="Editar"
                        >
                            <i class="fa fa-edit"></i>
                        </button>
                        <button
                            id="btnDelete${idxBody}" 
                            class="btn btn-danger btn-sm"
                            onclick = 'openDelete("`+cri+`")'
                            title="Eliminar"
                        >
                            <i class="fa fa-trash"></i>
                        </button>
                        <button
                            id="btnStock${idxBody}" 
                            class="btn btn-warning btn-sm"
                            data-toggle="modal" 
                            data-target="#mdlStock"
                            onclick = 'openStock("`+cri+`")'
                            title="Existencias"
                        >
                            <i class="fa fa-box"></i>
                        </button>
                    </td>
                `;
            }
            
            body += `<tr>${td}</tr>`;
            
        }
    }
    body += '</tr>';
    return {head,body}
}
function categorias(element, value=null){
    $.ajax({
        type: "get",
        url: $("#idEdit").val(),
        data: {method:'ver_categorias'},
        dataType: "json",
        success: function (response) {
          if(response.result){
            var el =  `<option value="*">***categoría***</option>`;
            for (let idx = 0; idx < response.data.length; idx++) {
              const element = response.data[idx];
              if(element.estado==="1"){
                el +=  `<option value="${element.id}" `+ ((value===element.nombre)?'selected':'') +`>${element.nombre}</option>`
              }                
            }
          }
          $(`#${element}`).html(el)
        },
        error: function(err){
          Swal.fire('Error inesperado', JSON.stringify(err), 'danger')
        }
    });
};
function procesarFormularioEdicionProducto(data){
    console.log(data)
    var form = '<form class="form-horizontal" id="editData"><div class="row">';
    categorias("categoriaEdit", data.categoria);
    form += `
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" name="nombreEdit" id="nombreEdit" placeholder="nombre" value="${data.nombre_producto}" class="form-control text-primary">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="number" name="precioEdit" id="precioEdit" placeholder="precio" value="${data.precio}" class="form-control text-primary">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" name="referenciaEdit" id="referenciaEdit" placeholder="referencia" value="${data.referencia}" class="form-control text-primary">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="number" name="pesoEdit" id="pesoEdit" placeholder="peso" value="${data.peso}" class="form-control text-primary">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <select name="categoriaEdit" id="categoriaEdit" class="form-control text-primary">
                    
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="hidden" name="method" id="method" value="actualizar_producto" >
                <input type="hidden" name="id" id="id" value="${data.id}" >
                <button type="submit" class="btn btn-primary">Actualizar registro</button>
            </div>
        </div>
        <script>
            $('body').on('submit','#editData', function(e){
                e.preventDefault();
                $.ajax({
                    type: "post",
                    url: $("#idEdit").val(),
                    data: $("#editData").serialize(),
                    dataType: "json",
                    success: function (response) {
                        if(response.result){
                          Swal.fire('Proceso exitoso', 'El producto se ha actualizado', 'success')
                        }else{
                          Swal.fire('Proceso detenido', 'El producto no se actualizó. '+response.help, 'warning')
                        }
                    },
                    error: function(err){
                        Swal.fire('Error inesperado', JSON.stringify(err), 'danger')
                    }
                });
            })
        </script>
    `;
        
    form += ' </div></form>';
    return form;
}
function formStock(){
    console.log("proce")
    $("body").on("submit", "#formStock", function(e){
        console.log("ewna");
        e.preventDefault();
        const idprod = $("#prodActual").val();
        $.ajax({
            type: "post",
            url: $("#idEdit").val(),
            data: $("#formStock").serialize()+`&id=${idprod}`,
            dataType: "json",
            success: function (response) {
                if(response.result){
                    Swal.fire('Proceso exitoso', 'Existencia registrada. ', 'success')
                }else{
                  Swal.fire('Proceso detenido', 'La existencia no se registró. '+response.help, 'warning')
                }
            },
            error: function(err){
            Swal.fire('Error inesperado', JSON.stringify(err), 'danger')
            }
        });
    })
}
function openStock(data){
    formStock();
    data = JSON.parse(data)
    $.ajax({
        type: "post",
        url: $("#idEdit").val(),
        data: {method:'lista_stock',id:data.id},
        dataType: "json",
        success: function (response) {
          if(response.result){
            const datosTabla = procesarTabla(response.data);
            const tabla = generarTabla(datosTabla, false);
            console.log(tabla)
            $("#tblStock").html(
            `<input value="${data.id}" id="prodActual" type="hidden"/>
            <thead>${tabla.head}</thead>
            <tbody>${tabla.body}</tbody>`
            ).DataTable();
          }
        },
        error: function(err){
        }
    });
}
function generarCatalogo(data){
    var card = '<div class="row">';
    if(data.length>0){
        for (let idxProd = 0; idxProd < data.length; idxProd++) {
            const element = data[idxProd];
            card += `
                <div class="col-lg-6">
                    <div class="card card-columns ">
                        <div class="card-header">
                            <div class="text-center align-self-center align-content-md-center">
                            <img 
                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATkAAAChCAMAAACLfThZAAAANlBMVEX////G18Tt7e37+/vy8vLI2MbZ5NjN3MzR3s/p7+jE1cLt8uzb5dnw9e/e6N36+vr1+PXj6+P5lGlhAAAGAElEQVR4nO2di5KjIBBFg4j4SET//2cXVBR8RO3MBlLcs7W1MxMn5Zxt6AaBeTwAACAxeJ5nYEOuTrzlWd7xr/wP/RhcZdkbeerdiyA/0sOz/Lt38nvk2d5XVYZmesqeJLWrE6zZtFiIu8paFMRdhfuq0MddR+VHn4AT3DBDW70DX+IMIXePnG8/AldYIg2N9SZWGBrrXWwjPZ1BASusMZi7S2fNIUHcxPZvSBB34TBHBOaowBwVmKMCc1RgjgrMUYE5KjBHBeaowBwVmKMCc1RgjgrMUYE5KjBHBeaowBwVmKMCc1R+1Vz36gLfwS+a41UhBRNF4Lv4MXOqaSVjQui/LKy6nzL36ksxapPPXor6FfJmfsZcN1pj2lrRmKUcHWNtyBv6CXNKd2xjrLGit5mBS1GGvKnozfGml7W2plNC2XrNU8dgyGVEcZsbm6jxNjVRl56xJshdjYQ11wxiDhnSwcL61fErogzjL6i5qt7o2Mg50Da9OPxTB1EX0hw3fVfxKSUTMsDNBzX3+pu6omUsxEgspLnmb7r4irEQJXFgc9UfvA3MUYE5Kgmaq6y5p6TR2rdJy1wjdaU7mOM1e1/WHRV74+grNXNNbQrZyZyQ5X1koub0z10uMUep654sSXNmdu3FYO4+2lx/y1y1GSckak63VvnOHF/NKRX1ZnCaqrlCiOrQ3KvU6aN15HU6+67tpmquZ6I4yhC9qTj0n6WBGpPrcX2q5jq3KvHN6VbMykI6s0dajtw8JUzV3EMcmpNCmDmUYhmc6UK50zWMP7OSrLmWHZjjYnx8rwR72ktZb7o6P0kka645Mjc/SJVT++x0dD6GGOzdN0jWXHfYWsX4IFUbHGNuGmuYyXe3VknWnMmX++Z0cBXcmB2zqR7hjo+ke+YliXTNtezAnHEm5LzkRop6qkekV5mka+51ZO7xksM00iiuXZ7zvLwkka65x6E5s0KufI7hxXUimXs3O7sykLC5S7NMhTvlrlyNCZvrd8etqnA1+g3Ubbopm2t2zZWsdso2HZeeFslsuoC5lblhaDFn0FUhMtTPdtWcY+4vHqHdJTZzr2GhzdxAtzMky/B1MRdksXVs5nTJ1hR28GDk9Ktv6mavjjnxhbtdE5m5QRqXU1xtBvnTJaNNx1wdYElOXOaqUZUesQpzLt5mYmm42k55OuZCrLWOypwS04KufsgD84DVx2YNt7UGWGsdlTkdY1O3Vpowkpv0MCLHSsU1J/jehf+VmMzpYLKx05ndSUcLE6fq2DUXoKKLyJzt3QYGHfIgksYRmWdunYL/PxGZk14+eLJjHd3QPD1z318qHI+5pYqbXnzT7femIXv9XP31U32jMVfVfu12lB5GdBJWzuirEN8fgMVijgt/hflmwLr+VlE8l1VglR+vXyEOc6It/V6Ni5NNXaUehDnr5+T390REYk4sBcnAzoDVpxs2L83mWncpxXeIxZxfy+4OWH1ad7Vr90zXHFtvwDzdY+LEnChFuq1VeEuA15/vLxMWblXy9V1zcZjb7s48X61u1onNcyXy+9sNYzD3+X6IVGfTPyThJzgfAnNU0jSH/a0kXmcDhWskuKfaPIv5eBt/kvv457Mj9k+QeHNshH9ZemdHaHVyOshl5yySi+LSPK9kYqeUVU1hjyMp+9Bn9O0Tgzm1msXszGyd8VbLfnOmUDTEYO7hzEt2VTF1fLKoov6NgVGYm2bXeGNOgRxOY4q1iTpEYc4sg6ie9hTIsg96luFVojD3KJk9BbJtom6iDnGY4yYl6I4t2nSwQxzmYjgf+C6xmPs9YI4KzFGBOSowRwXmqMAcFZijAnNUYI4KzFGBOSowRwXmqMAcFZijAnNUYI4KzFGBOSowRwXmqKjJXP5LjzqjYDb3K4/Wo0FNxjqYu4mNNRt74CqZ7d+QIm4yC0OKuMfSSNFc75HxvQ/BKW6gIeju4KWFDIXJZXI/zJBer6JWqjjUXWMraq0S7MJ3kilHX3fOQXxlyLDv4fmRIQV3b+D5u7JXZVmuUBZv4CrPTnszleuLgE+OOUwAQHr8A8sgPI8H5o4vAAAAAElFTkSuQmCC" 
                                title="${element.nombre_producto}" 
                                class="img-thumbnail image img-fluid "
                                style="width: 100%;"
                            />
                            <p class="text-success text-lg-center">${element.nombre_producto}</p>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    $ ${element.precio}
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                     ${element.categoria}
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                     ${element.peso} Kg
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    ref: ${element.referencia}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <form class="form-horizontal" id="formTienda${element.id}">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <input type="number" name="cantidad" id="cant${element.id}" value="0">
                                        <input type="hidden" name="id" value="${element.id}">
                                        <input type="hidden" name="method" value="generar_venta">
                                        <button type="submit" class="btn btn-success btn-sm float-end"><i class="fa fa-shopping-cart"></i> Añadir</button>
                                    </div>
                                </div>
                            </form>
                            <script>
                                $("body").on("submit", "#formTienda${element.id}", function(e){
                                    e.preventDefault();
                                    let cantidad = $("#cant${element.id}").val();
                                    let comprador = $("#comprador").val();
                                    if(cantidad<=0){
                                        Swal.fire('¡Disculpe!','Por favor seleccione la cantidad que desea', 'info')
                                        return false;
                                    }
                                    if(comprador===undefined||comprador===null||comprador===''){
                                        Swal.fire('¡Disculpe!','Por favor registre el comprador', 'info')
                                        return false;
                                    }
                                    $.ajax({
                                        type: "post",
                                        url: $("#idEdit").val(),
                                        data: $("#formTienda${element.id}").serialize()+'&comprador='+comprador,
                                        dataType: "json",
                                        success: function (response) {
                                            if(response.result){
                                                Swal.fire('Proceso exitoso', 'La venta se ha generado sin novedad', 'success')
                                            }else{
                                                Swal.fire('Proceso detenido', 'No se completo la venta. '+response.help, 'warning')
                                            }
                                        },
                                        error: function(err){
                                            Swal.fire('Error inesperado', JSON.stringify(err), 'danger')
                                        }
                                    });
                                })
                            </script>
                        </div>
                    </div>
                </div>
            `;
            
        }
        
    }else{
        card += `
        <div class="col-lg-12">
            <div class="card ">
                <div class="card-body">
                   No hay productos
                </div>
            </div>
        </div>
        `;
    }
    
    card += '</div>';
    $("#loadProductos").html(card)
}
function cargarProductos(){
    $.ajax({
        type: "get",
        url: $("#idEdit").val(),
        data: {method:"cargar_productos"},
        dataType: "json",
        success: function (response) {
            if(response.result){
                generarCatalogo(response.data)
            }else{
                Swal.fire('Proceso detenido', 'Hubo un problema al cargar los productos. '+response.help, 'warning')
            }
        },
        error: function(err){
            Swal.fire('Error inesperado', JSON.stringify(err), 'danger')
        }
    });
}