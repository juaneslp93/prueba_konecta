# Prueba Konecta

Software diseñado para la presentación de un mínimo producto viable, tomando como módulos productos y ventas.

## Versión de PHP 8.1.1

## Instalación

Descarga el respositorio desde:

```bash
git clone https://gitlab.com/juaneslp93/prueba_konecta.git
```
Posteriormente importa la base de datos prueba_konecta.sql ubicada en la carpeta database
## Uso

Corre el servidor apache y el motor de base de datos mysql desde el panel de control del xampp y ubica en tu navegador la url [http://localhost/prueba_konecta/](http://localhost/prueba_konecta/).


## License

[MIT](https://choosealicense.com/licenses/mit/)