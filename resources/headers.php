<?php 
$sandbox = true;
$mostrarErrores = false;
error_reporting(E_ALL);
ini_set("display_errors", (($mostrarErrores)?1:0));
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization, SksToken"); # Encabezados permitidos
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE"); # methodos permitidos
header("X-XSS-Protection: 1; mode=block");
header('X-Frame-Options: SAMEORIGIN');
header("Strict-Transport-Security:max-age=63072000");
header("Set-Cookie: name=value; httpOnly");
header("Set-Cookie: name=value; secure");
header("Content-Type: application/json"); # tipo de archivo e información
header("charset=utf-8"); # juego de caracteres
header("Allow: GET, POST, OPTIONS, PUT, DELETE"); # metodos a recibir */
// header("Authorization: application/json"); # autorización de aplicación
$cspHeader = "Content-Security-Policy:".
"frame-ancestors 'self';".
"form-action 'self' ;".
"img-src 'self' 'unsafe-inline' data:  ;".
"connect-src 'self' 'unsafe-inline' ;".
"style-src 'self' 'unsafe-inline' ;".
"font-src 'self' 'unsafe-inline' ;".
"script-src 'self' 'unsafe-inline' ;".
"default-src 'none'; base-uri 'none';".
"form-action 'none';".
"frame-src 'self' 'unsafe-inline' ;".
"object-src 'self'";
header($cspHeader);
$request = $_SERVER['REQUEST_METHOD']; # tipo de solicitud
$params = (!empty($_POST)?$_POST:$_GET); # paramtros de solicitud
if($request=="POST" && empty($params)) $params = json_decode(file_get_contents("php://input"), true);
$fechaSystem = date("Y-m-d H:m:s"); # fecha global del sistema (para hacer uso de ella solo es invocar $GLOBALS["fechaSystem"])


?>