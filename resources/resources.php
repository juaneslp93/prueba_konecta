<?php 
    date_default_timezone_set('America/Bogota');
    require_once __DIR__."/headers.php";
    require_once __DIR__."/../controller/ThrowControl.php";
    require_once __DIR__."/../database/Db.php";
    require_once __DIR__."/../models/Model.php";
    require_once __DIR__."/../controller/Controller.php";   
?>